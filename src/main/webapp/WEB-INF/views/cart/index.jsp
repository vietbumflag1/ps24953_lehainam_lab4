<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 22/05/2023
  Time: 4:55 CH
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">

    <table class="table" >
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Amount</th>
            <th></th>
        </tr>
        <c:forEach var="item" items="${cart.items}">
            <form action="/cart/update/${item.id}" method="post">
                <input type="hidden" name="id" value="${item.id}">
                <tr>
                    <td>${item.id}</td>
                    <td>${item.name}</td>
                    <td>${item.price}</td>
                    <td>
                            <%--                    <button id="increaseBtn">+</button>--%>
                        <input type="number" id="quantityInput" name="qty" value="${item.qty}"
                               onblur="this.form.submit()" style="width:50px;" min="1" max="3">
                            <%--                    <button id="decreaseBtn">-</button>--%>
                    </td>

                    <td>${item.price * item.qty}</td>
                    <td>
                        <a href="/cart/remove/${item.id}">Remove</a>
                    </td>
                </tr>
            </form>
        </c:forEach>
    </table>
    <a href="/cart/clear">Clear Cart</a>
    <a href="/item/index">Add more</a>
    <a>${errormessage}</a>
</div>
</body>
</html>
