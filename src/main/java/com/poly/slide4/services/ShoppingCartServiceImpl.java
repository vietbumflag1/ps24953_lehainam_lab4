package com.poly.slide4.services;

import com.poly.slide4.controller.DB;
import com.poly.slide4.model.Item;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import java.util.*;

@SessionScope
@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    private static List<Item> orders = new ArrayList<>();

    @Override
    public Item add(Integer id) {
        Item item = DB.items.get(id);
        if (item != null) {
            boolean found = false;
            for (Item existingItem : orders) {
                if (existingItem.getId() == item.getId()) {
                    if (existingItem.getQty()>3){
                        System.out.println("u cannot add more product");
                    }else{
                        // Sản phẩm đã tồn tại, tăng số lượng
                        existingItem.setQty(existingItem.getQty() + 1);
                        found = true;
                        break;
                    }
                }
            }
            if (!found) {
                // Sản phẩm chưa tồn tại, thêm mới vào danh sách
                item.setQty(1);
                orders.add(item);
            }
        }
        return item;
    }


    @Override
    public void remove(Integer id) {
        Item foundItem = null;
        for (Item item : orders) {
            if (item.getId() == id) {
                foundItem = item;
                break;
            }
        }

        if (foundItem != null) {
            // Giảm số lượng sản phẩm đã chọn
            foundItem.setQty(foundItem.getQty() - 1);

            if (foundItem.getQty() == 0) {
                // Nếu số lượng sản phẩm bằng 0, xóa khỏi danh sách
                orders.remove(foundItem);
            }
        }
    }


    @Override
    public Item update(Integer id, int qty) {
        Item updatedItem = null;

        for (Item item : orders) {
            if (item.getId() == id) {
                updatedItem = item;
                break;
            }
        }

        if (updatedItem != null) {
            Item prodCheck = DB.items.get(id);

            if (prodCheck != null) {
                // Cập nhật số lượng sản phẩm
                updatedItem.setQty(qty);

                if (qty == 0) {
                    // Nếu số lượng sản phẩm là 0, xóa khỏi danh sách
                    orders.remove(updatedItem);
                }
            }
        }

        return updatedItem;
    }


    @Override
    public void clear() {
        orders.clear();
    }


    @Override
    public Collection<Item> getItems() {
        return (Collection<Item>) orders;
    }

    @Override
    public int getCount() {
        int count = 0;
        for (Item prod : orders) {
            count += prod.getQty();
        }
        return count;
    }

    @Override
    public double getAmount() {
        double amount = 0;
        for (Item prod : orders) {
            amount += prod.getPrice() * prod.getQty();
        }
        return amount;
    }

}
